package com.arc.statistic.modules.camera.service;


import com.arc.statistic.innerservice.entity.AddressEntity;
import com.arc.statistic.modules.camera.entity.CameraEntity;

/**
 * @author wjh
 */
public interface ICameraService {
    /**
     * @param record
     * @return
     */
    boolean saveOrUpdate(CameraEntity record);

    /**
     * 获取设备token
     *
     * @param record
     * @return
     */
    CameraEntity getCameraWithToken(CameraEntity record);

    /**
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param protocol 流播放协议，1-ezopen、2-hls、3-rtmp，默认为1
     * @return
     */
    AddressEntity address(String id, Integer quality,Integer protocol,Integer expireTime);

    /**
     * 开始-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    方向 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @param channelNo    通道 默认1
     * @return
     */
    void start(String id, Integer direction,Integer speed,Integer channelNo);

    /**
     * 停止-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @param channelNo    通道 默认1
     * @return
     */
    void stop(String id, Integer direction,Integer channelNo);

    /**
     * 镜像-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    镜像方向：2--(只能是2，每执行一次翻转一次)
     * @return
     */
    void mirror(String id,Integer channelNo);
}
