package com.arc.statistic.modules.camera.controller;

import com.arc.statistic.modules.camera.entity.CameraEntity;
import com.arc.statistic.modules.camera.service.ICameraService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dhcloud.avap.exception.ServiceException;
import com.dhcloud.avap.operation.annotation.Operation;
import com.dhcloud.avap.status.BaseErrorCodeSuit;
import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 传感器对外接口
 *
 * @author wjh
 */
@RestController
@RequestMapping("camera")
public class CameraController extends RestControllerSupport {

    final ICameraService cameraService;

    @Autowired
    public CameraController(ICameraService cameraService) {
        this.cameraService = cameraService;
    }

    /**
     * 删除，支持多个
     *
     * @param ids 用户ids
     * @return @
     */
    @DeleteMapping(value = "/{ids}")
    @Operation(value = "删除摄像头", paramNames = "ids")
    public RestResponse delete(@PathVariable String[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            return restResponses.error(BaseErrorCodeSuit.DefaultError.ILLEGAL_ARGUMENT.getStatus());
        }
        for (String id : ids) {
            new CameraEntity().deleteById(id);
        }
        return restResponses.ok();
    }

    /**
     * 新增摄像头
     *
     * @param param @
     * @return @
     */
    @PostMapping
    @Operation(value = "新增摄像头", paramNames = "param")
    public RestResponse add(@RequestBody CameraEntity param) {
        Date now = new Date();
        param.setCreateTime(now)
                .setUpdateTime(now);
        return param.insert() ? restResponses.ok(param) : restResponses.error();
    }


    /**
     * 修改
     *
     * @param param @
     * @return @
     */
    @PutMapping
    @Operation(value = "更新摄像头", paramNames = "param")
    public RestResponse update(@RequestBody CameraEntity param) {
        if (StringUtils.isBlank(param.getId())) {
            throw new ServiceException(BaseErrorCodeSuit.DefaultError.ILLEGAL_ARGUMENT.getStatus());
        }
        param.setUpdateTime(new Date());
        return param.updateById() ? restResponses.ok(param) : restResponses.error();
    }

    /**
     * 分页查询
     *
     * @param param @
     * @param page  @
     * @return @
     */
    @GetMapping
    public RestResponse list(CameraEntity param, Page<CameraEntity> page) {
        logger.info("传入参数: param: {} page: {}", param, page);
        QueryWrapper<CameraEntity> query = new QueryWrapper<>();
        query.apply(StringUtils.isNotBlank(param.getKeyWord()),
                " ( id like '%" + param.getKeyWord() + "%' " +
                        "or name like '%" + param.getKeyWord() + "%' " +
                        "or app_key like '%" + param.getKeyWord() + "%'" +
                        "or app_secret like '%" + param.getKeyWord() + "%' " +
                        "or device_serial like '%" + param.getKeyWord() + "%' " +
                        "or validate_code like '%" + param.getKeyWord() + "%' )");
        return restResponses.ok(param.selectPage(page, query));
    }

    /**
     * @param id 设备ID
     * @return @
     */
    @GetMapping("address")
    public RestResponse address(String id, Integer quality, Integer protocol, Integer expireTime) {
        return restResponses.ok(cameraService.address(id, quality, protocol, expireTime));
    }

    /**
     * 开始-云台控制
     *
     * @param id        设备ID
     * @param direction 方向 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @param channelNo 通道，默认1
     * @return @
     */
    @PostMapping("start")
    public RestResponse start(String id, Integer direction, Integer speed, Integer channelNo) {
        cameraService.start(id, direction, speed, channelNo);
        return restResponses.ok();
    }

    /**
     * 停止-云台控制
     *
     * @param id        设备ID
     * @param direction 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @param channelNo 通道，默认1
     * @return @
     */
    @PostMapping("stop")
    public RestResponse stop(String id, Integer direction, Integer channelNo) {
        cameraService.stop(id, direction, channelNo);
        return restResponses.ok();
    }

    /**
     * 镜像-云台控制
     *
     * @param id 设备ID
     * @return @
     */
    @PostMapping("mirror")
    public RestResponse mirror(String id, Integer channelNo) {
        cameraService.mirror(id, channelNo);
        return restResponses.ok();
    }
}
