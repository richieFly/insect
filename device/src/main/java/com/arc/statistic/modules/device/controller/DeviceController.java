package com.arc.statistic.modules.device.controller;

import com.arc.statistic.modules.device.entity.DeviceEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 设备对外接口
 *
 * @author wjh
 */
@RestController
@RequestMapping("device")
public class DeviceController extends RestControllerSupport {

    /**
     * 分页查询
     * @param param @
     * @param page @
     * @return
     */
    @GetMapping
    public RestResponse list(DeviceEntity param, Page<DeviceEntity> page) {
        logger.info("传入参数: param: {} page: {}", param ,page);
        QueryWrapper<DeviceEntity> query = new QueryWrapper<>();
        query.apply(StringUtils.isNotBlank(param.getKeyWord()),"id like '%" + param.getKeyWord() + "%' ");
        return restResponses.ok(param.selectPage(page, query));
    }

}
