package com.arc.statistic.modules.sensor.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author axx
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_sensor")
public class SensorEntity extends Model<SensorEntity> {

    /**
     * 传感器ID
     */
    @TableId
    private String sensorId;
    @Override
    protected Serializable pkVal() {
        return sensorId;
    }

    private String deviceId;

    /**
     * 报警类型
     */
    private Integer alarmType;

    /**
     * 数据值
     */
    private Double dataValue;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 传感器类型
     */
    private String sensorType;

    /**
     * 传感器名称（英）
     */
    private String nameEn;

    /**
     * 传感器名称（中）
     */
    private String nameCn;

    /**
     * 单位
     */
    private String unit;

    /**
     * 查询关键字
     */
    @TableField(exist = false)
    private String keyWord;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Timestamp createTime;

    /**
     * 更新时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Timestamp updateTime;
}

