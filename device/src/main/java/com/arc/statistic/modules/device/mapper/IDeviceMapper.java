package com.arc.statistic.modules.device.mapper;

import com.arc.statistic.modules.device.entity.DeviceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjh
 */
@Mapper
public interface IDeviceMapper extends BaseMapper<DeviceEntity> {

}
