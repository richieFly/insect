package com.arc.statistic.modules.main.service.impl;

import com.arc.statistic.modules.device.entity.DeviceEntity;
import com.arc.statistic.modules.main.entity.MainEntity;
import com.arc.statistic.modules.main.service.IMainService;
import com.arc.statistic.modules.sensor.entity.SensorEntity;
import com.arc.statistic.modules.sensor.enums.SensorType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Objects;

/**
 * 账户余额实现类
 *
 * @author wjh
 */
@Service
@Slf4j
public class MainServiceImpl implements IMainService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(MainEntity param) {
        // 存储设信息
        DeviceEntity device = param.getDevice();

        DeviceEntity entityOld = device.selectById(device.getDeviceId());
        if (Objects.nonNull(entityOld)) {
            device.setUpdateTime(param.getTimestamp());
            device.updateById();
            log.info("设备 ---------> 更新完成");
        } else {
            device.insert();
            log.info("设备 ---------> 新增完成");
        }
        // 存储传感器信息
        param.getSensorData().forEach(sensor -> {
            SensorEntity sensorOld = sensor.selectById(sensor.getSensorId());
            sensor.setDeviceId(device.getDeviceId());
            if (Objects.nonNull(sensorOld)) {
                sensor.updateById();
            } else {
                String typeStr = sensor.getSensorId();
                if(StringUtils.isNoneEmpty(typeStr)){
                    String typeName = typeStr.substring(typeStr.length() -2);
                    Arrays.stream(SensorType.values()).forEach(val->{
                        if(val.name().equalsIgnoreCase(typeName)){
                            sensor.setSensorType(val.name());
                            sensor.setNameEn(val.getNameEn());
                            sensor.setNameCn(val.getNameCn());
                            sensor.setUnit(val.getUnit());
                        }
                    });

                }
                sensor.insert();
            }
        });
        log.info("传感器 ---------> 存储完成");
    }
}
