package com.arc.statistic.modules.main.controller;

import com.arc.statistic.modules.main.entity.MainEntity;
import com.arc.statistic.modules.main.service.IMainService;
import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author axx
 */
@RestController
@Slf4j
public class MainController extends RestControllerSupport {

    final IMainService mainService;

    @Autowired
    public MainController(IMainService mainService) {
        this.mainService = mainService;
    }

    @RequestMapping("/")
    public RestResponse add(@RequestBody MainEntity param) {
        log.info("传入参数: {}", param);
        mainService.add(param);
        return restResponses.ok();
    }

    // @GetMapping("list")
    // public RestResponse list(MainEntity param) {
    //     // /*
    //     //  * 分页查询
    //     //  */
    //     // Page<MainEntity> page = new Page<>();
    //     // // 页尺寸
    //     // page.setSize(10);
    //     // // 当前页码
    //     // page.setCurrent(2);
    //     // QueryWrapper<AccountBalanceEntity> query = new QueryWrapper<>();
    //     // query.setEntity(param);
    //     // page = param.selectPage(page,query);
    //     //
    //     // log.info("传入参数: {}", param);
    //     // mainService.add(param);
    //     // return restResponses.ok();
    // }



}
