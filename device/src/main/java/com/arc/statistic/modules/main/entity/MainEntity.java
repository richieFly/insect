package com.arc.statistic.modules.main.entity;

import com.arc.statistic.modules.device.entity.DeviceEntity;
import com.arc.statistic.modules.sensor.entity.SensorEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author axx
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MainEntity {
    /**
     * 设备信息
     */
    private DeviceEntity device;

    /**
     * 传感器信息
     */
    private List<SensorEntity> sensorData;

    /**
     * 更新时间
     */
    private Timestamp timestamp;
}
