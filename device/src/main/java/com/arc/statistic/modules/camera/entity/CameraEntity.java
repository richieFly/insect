package com.arc.statistic.modules.camera.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author axx
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_camera")
public class CameraEntity extends Model<CameraEntity> {

    /**
     * 唯一ID
     */
    @TableId
    private String id;
    @Override
    protected Serializable pkVal() {
        return id;
    }

    /**
     * 设备名称
     */
    @NotNull
    private String name;

    /**
     * 秘钥1
     */
    @NotNull
    private String appKey;

    /**
     * 秘钥2
     */
    @NotNull
    private String appSecret;

    /**
     * 序列号
     */
    @NotNull
    private String deviceSerial;

    /**
     * 验证码
     */
    @NotNull
    private String validateCode;

    /**
     * token
     */
    private String accessToken;

    /**
     * 查询关键字
     */
    @TableField(exist = false)
    private String keyWord;

    /**
     * token过期时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Date expireTime;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Date updateTime;
}

