package com.arc.statistic.modules.sensor.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author wjh
 */

public enum SensorType {
    /**
     * 土壤温度
     */
    A1("土壤温度","soil_temp","℃"),
    A2("土壤水分","soil_water","%"),
    A3("土壤盐分","soil_salinity","ms/cm"),
    A4("土壤PH值","soil_ph","NULL"),
    A5("土壤紧实度","soil_hard","g"),
    A6("雪深","snow_depth","mm"),
    A7("果实膨大","fruit_enlargement","mm"),
    A8("噪声","noise","dB"),
    AA("叶面温度","leaf_temp","℃"),
    AB("茎秆微变","stem_changes","mm"),
    AC("土壤水势","soil_water_flow","kpa"),
    AD("土壤热通量","soil_heat_flux","W/m2"),
    B0("日降雨量","rain_day","mm"),
    B1("空气温度","air_water_1","℃"),
    B2("空气湿度","air_water_2","%RH"),
    B3("露点温度","der_temp","℃"),
    B4("风向","wind_direction","o"),
    B5("风速","wind_speed","m/s"),
    B6("雨量","rain_fall","mm"),
    B7("蒸发","evaporation","mm"),
    B8("大气压","atmosphere","HPa"),
    B9("二氧化碳","co2","ppm"),
    BA("光照强度","light_intensity","Lux"),
    BB("光合有效射","photo_synthetic","umol"),
    BC("紫外辐射","ultraviolet_radiation","uw/c㎡"),
    BD("总辐射","total_radiation","W/M2"),
    BE("叶面湿度","leaf_wet","%RH"),
    BF("日照时数","sunshine_hours","h"),
    C0("二氧化碳","co2","PPM"),
    C2("一氧化碳","co","mg/m3"),
    C3("氨","nh3","ppm"),
    C5("PM2.5","PM2.5","ug/m3"),
    C6("PM10","PM10","ug/m3"),
    C7("二氧化硫","sulfur_dioxide","ppb"),
    C8("空气负离子","air_ions","个/cm3"),
    C9("浊度","muddy","NTU"),
    D3("水体PH","water_ph"," "),
    D4("水温","water_temp","℃"),
    D5("溶解氧","O2","mg/L"),
    D6("水体氨氮","water_ammonia_nitrogen","mg/L"),
    D7("ORP","ORP","mv"),
    D8("水体电导","water_conductivity","ms"),
    D9("水位","water_level","mm"),
     ;
    /**
     * 传感器名称（中）
     */
    @Getter
    @Setter
    private String nameCn;

    /**
     * 传感器名称（英）
     */
    @Getter
    @Setter
    private String nameEn;
    /**
     * 单位
     */
    @Getter
    @Setter
    private String unit;


    SensorType(String nameCn, String nameEn, String unit) {
        this.nameCn = nameCn;
        this.nameEn = nameEn;
        this.unit = unit;
    }
}
