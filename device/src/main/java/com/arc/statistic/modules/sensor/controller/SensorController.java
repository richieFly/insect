package com.arc.statistic.modules.sensor.controller;

import com.arc.statistic.modules.sensor.entity.SensorEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dhcloud.avap.exception.ServiceException;
import com.dhcloud.avap.status.BaseErrorCodeSuit;
import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 传感器对外接口
 *
 * @author wjh
 *
 */
@RestController
@RequestMapping("sensor")
public class SensorController extends RestControllerSupport {

    /**
     * 分页查询
     * @param param @
     * @return
     */
    @GetMapping
    public RestResponse list(SensorEntity param) {
        logger.info("传入参数: param: {}", param);
        if(StringUtils.isBlank(param.getDeviceId())){
            throw new ServiceException(BaseErrorCodeSuit.DefaultError.ILLEGAL_ARGUMENT.getStatus());
        }
        QueryWrapper<SensorEntity> queryWrapper = new QueryWrapper<>(param);
        return restResponses.ok(param.selectList(queryWrapper));
    }

}
