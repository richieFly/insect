package com.arc.statistic.modules.device.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author axx
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_device")
public class DeviceEntity extends Model<DeviceEntity> {
    /**
     * 设备ID
     */
    @TableId
    private String deviceId;
    @Override
    protected Serializable pkVal() {
        return deviceId;
    }

    /**
     * 设备类型
     */
    private Integer deviceType;

    /**
     * 重启
     */
    private Boolean reboot;

    /**
     * 信号强度
     */
    private Double signalStrength;

    /**
     * 电池电压 伏特
     */
    private Double batteryVoltage;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 太阳能电压 伏特
     */
    private Double solarVoltage;

    /**
     * 上报时间间隔 毫秒
     */
    private Double reportInterval;

    /**
     * 地块ID
     */
    private String landId;

    /**
     * 查询关键字
     */
    @TableField(exist = false)
    private String keyWord;

    /**
     *
     */
    private Boolean selfProductionSm;

    /**
     * 经度
     */
    private Double axisX;

    /**
     * 维度
     */
    private Double axisY;

    /**
     * 采样率
     */
    private Double snapInterval;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Timestamp createTime;

    /**
     * 更新时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
    private Timestamp updateTime;
}