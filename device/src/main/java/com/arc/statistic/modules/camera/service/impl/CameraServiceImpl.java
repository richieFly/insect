package com.arc.statistic.modules.camera.service.impl;


import cn.hutool.http.HttpStatus;
import com.arc.statistic.common.Response;
import com.arc.statistic.innerservice.CameraClient;
import com.arc.statistic.innerservice.entity.AddressEntity;
import com.arc.statistic.innerservice.entity.TokenEntity;
import com.arc.statistic.modules.camera.entity.CameraEntity;
import com.arc.statistic.modules.camera.service.ICameraService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

/**
 * @author wjh
 * @date 2020/12/23 12:36
 */
@Service
@Slf4j
public class CameraServiceImpl implements ICameraService {
    final CameraClient cameraClient;

    @Autowired
    public CameraServiceImpl(CameraClient cameraClient) {
        this.cameraClient = cameraClient;
    }

    @Override
    public boolean saveOrUpdate(CameraEntity record) {
        return record.insertOrUpdate();
    }

    @Override
    public CameraEntity getCameraWithToken(CameraEntity record) {
        record = record.selectById();
        if (Objects.isNull(record.getExpireTime()) || record.getExpireTime().before(new Date()) || StringUtils.isBlank(record.getAccessToken())) {
            Response<TokenEntity> response = cameraClient.token(record.getAppKey(), record.getAppSecret());
            if (response.getCode() == HttpStatus.HTTP_OK) {
                TokenEntity tokenEntity = response.getData();
                record.setAccessToken(tokenEntity.getAccessToken())
                        .setExpireTime(tokenEntity.getExpireTime())
                        .updateById();
            }
        }
        return record;
    }

    @Override
    public AddressEntity address(String id, Integer quality, Integer protocol,Integer expireTime) {
        log.info("获取直播播放地址:id{}, quality:{},protocol:{}, expireTime:{}",id, quality, protocol, expireTime);
        CameraEntity record = new CameraEntity();
        record.setId(id);
        // 视频清晰度，1-高清（主码流）、2-流畅（子码流）
        quality = Objects.isNull(quality) ? 1 : quality;
        // 流播放协议，1-ezopen、2-hls、3-rtmp，默认为1
        protocol = Objects.isNull(protocol) ? 3 : protocol;
        // 过期时长，单位秒；针对hls/rtmp设置有效期，相对时间；30秒-7天,默认3600(3600分钟)
        expireTime = Objects.isNull(expireTime) ? 3600 : expireTime;
        record = this.getCameraWithToken(record);
        Response<AddressEntity> res = cameraClient.address(record.getAccessToken(), record.getDeviceSerial(), quality,protocol,expireTime);
        log.info("获取直播播放地址：res:{}", res);
        return res.getCode() == HttpStatus.HTTP_OK ? res.getData() : null;
    }

    @Override
    public void start(String id, Integer direction,Integer speed,Integer channelNo) {
//        direction 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
//        speed 云台速度：0-慢，1-适中，2-快，海康设备参数不可为0
        CameraEntity record = new CameraEntity();
        record.setId(id);
        record = this.getCameraWithToken(record);
        speed = Objects.isNull(speed) ? 1 : speed;
        channelNo = Objects.isNull(channelNo) ? 1 : channelNo;
        cameraClient.start(record.getAccessToken(), record.getDeviceSerial(), channelNo, direction,speed);
    }

    @Override
    public void stop(String id, Integer direction,Integer channelNo) {
        CameraEntity record = new CameraEntity();
        record.setId(id);
        record = this.getCameraWithToken(record);
        channelNo = Objects.isNull(channelNo) ? 1 : channelNo;
        cameraClient.stop(record.getAccessToken(), record.getDeviceSerial(), direction,channelNo);
    }

    @Override
    public void mirror(String id,Integer channelNo) {
        CameraEntity record = new CameraEntity();
        record.setId(id);
        record = this.getCameraWithToken(record);
        channelNo = Objects.isNull(channelNo) ? 1 : channelNo;
        cameraClient.mirror(record.getAccessToken(), record.getDeviceSerial(),channelNo,2);
    }

}
