package com.arc.statistic.modules.camera.mapper;

import com.arc.statistic.modules.camera.entity.CameraEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjh
 */
@Mapper
public interface ICameraMapper extends BaseMapper<CameraEntity> {

}
