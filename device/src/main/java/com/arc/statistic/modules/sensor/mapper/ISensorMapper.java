package com.arc.statistic.modules.sensor.mapper;

import com.arc.statistic.modules.sensor.entity.SensorEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjh
 */
@Mapper
public interface ISensorMapper extends BaseMapper<SensorEntity>  {

}
