package com.arc.statistic.modules.main.service;

import com.arc.statistic.modules.main.entity.MainEntity;

/**
 * @author wjh
 */
public interface IMainService {
    /**
     * 新增数据
     * @param record @
     * @return @
     */
    void add(MainEntity record);
}
