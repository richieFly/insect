package com.arc.statistic.innerservice.entity;

import lombok.Data;

/**
 * @author axx
 */
@Data
public class AddressEntity {
    /**
     * 唯一ID
     */
    private String id;

    /**
     * URL
     */
    private String url;
    /**
     * 过期时间
     */
    private String expireTime;


}
