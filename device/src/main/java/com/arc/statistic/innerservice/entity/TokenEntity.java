package com.arc.statistic.innerservice.entity;


import lombok.Data;

import java.sql.Timestamp;

/**
 * @author wjh
 * @date 2020/12/23 13:30
 */
@Data
public class TokenEntity {

    /**
     * 过期时间
     */
    private Timestamp expireTime;

    /**
     * token
     */
    private String accessToken;
}
