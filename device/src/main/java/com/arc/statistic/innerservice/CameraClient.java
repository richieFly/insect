package com.arc.statistic.innerservice;

import com.arc.statistic.common.Response;
import com.arc.statistic.innerservice.entity.AddressEntity;
import com.arc.statistic.innerservice.entity.TokenEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wjh
 */
@FeignClient(name = "CameraClient" ,url = "${device.cameraApi}")
public interface CameraClient {

    /**
     * 获取用户token
     *
     * @param appKey    k1
     * @param appSecret k2
     * @return
     */
    @PostMapping("/token/get")
    Response<TokenEntity> token(@RequestParam("appKey") String appKey, @RequestParam("appSecret") String appSecret);

    /**
     * 获取播放地址
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param quality      质量级别 视频清晰度，1-高清（主码流）、2-流畅（子码流）
     * @param protocol     流播放协议，1-ezopen、2-hls、3-rtmp，默认为1
     * @param expireTime   过期时长，单位秒；针对hls/rtmp设置有效期，相对时间；30秒-7天
     * @return @
     */
    @PostMapping("/v2/live/address/get")
    Response<AddressEntity> address(@RequestParam("accessToken") String accessToken, @RequestParam("deviceSerial") String deviceSerial, @RequestParam("quality") Integer quality, @RequestParam("protocol") Integer protocol, @RequestParam("expireTime") Integer expireTime);

    /**
     * 开始-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    方向 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @return @
     */
    @PostMapping("/device/ptz/start")
    Response start(@RequestParam("accessToken") String accessToken, @RequestParam("deviceSerial") String deviceSerial, @RequestParam("channelNo") Integer channelNo, @RequestParam("direction") Integer direction, @RequestParam("speed") Integer speed);

    /**
     * 停止-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
     * @return @
     */
    @PostMapping("/device/ptz/stop")
    Response stop(@RequestParam("accessToken") String accessToken, @RequestParam("deviceSerial") String deviceSerial, @RequestParam("direction") Integer direction, @RequestParam("channelNo") Integer channelNo);

    /**
     * 镜像-云台控制
     *
     * @param accessToken  请求token
     * @param deviceSerial 序列号
     * @param direction    镜像方向：2--(只能是2，每执行一次翻转一次)
     * @return @
     */
    @PostMapping("/device/ptz/mirror")
    Response mirror(@RequestParam("accessToken") String accessToken, @RequestParam("deviceSerial") String deviceSerial, @RequestParam("channelNo") Integer channelNo, @RequestParam("command") Integer command);
}
