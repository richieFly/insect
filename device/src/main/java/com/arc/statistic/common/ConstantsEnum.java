package com.arc.statistic.common;

import lombok.Getter;
import lombok.Setter;

/**
 * @author wjh
 */

public enum ConstantsEnum {

    /**
     * 账单结果状态
     */
    BILLING_RESULT_STATUS_CREATE("已创建", 0),

    ;

    /**
     * 成员变量 代码
     */
    @Getter
    @Setter
    private int code;

    /**
     * 成员变量
     */
    @Getter
    @Setter
    private String name;

    ConstantsEnum(String name, int code) {
        this.name = name;
        this.code = code;
    }

    public static String getName(int code) {
        for (ConstantsEnum i : ConstantsEnum.values()) {
            if (i.getCode() == code) {
                return i.name;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return String.valueOf(this.code);
    }

}