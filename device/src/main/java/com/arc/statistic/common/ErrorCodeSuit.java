package com.arc.statistic.common;

import com.dhcloud.avap.status.BaseErrorCodeSuit;
import com.dhcloud.avap.status.DefaultStatus;
import com.dhcloud.avap.status.Status;
import org.springframework.stereotype.Component;

/**
 * 错误码
 *
 * @author dcz
 */
@Component
public class ErrorCodeSuit extends BaseErrorCodeSuit {

	public enum ErrorCode {


		/*
		 * ##################################频道操作错误begin############################
		 */
		/**
		 * 频道名称重复
		 */
		LIVE_CHANNEL_REPEAT_NAME(1001),
		;
		/**
		 * 组装错误码
		 *
		 * @return
		 */
		public Status getStatus() {
			return DefaultStatus.toStatus(this.code, message(this.name()));
		}

		/**
		 * 错误码
		 */
		private final Integer code;

		/**
		 * 构造方法
		 *
		 * @param code 错误码
		 */
		ErrorCode(Integer code) {
			this.code = fullCode(code);
		}
	}
}
