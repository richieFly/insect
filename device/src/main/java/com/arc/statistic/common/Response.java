package com.arc.statistic.common;

import lombok.Data;

/**
 * @author wjh
 * @date 2020/12/23 13:26
 */
@Data
public class Response<T> {
        private int code = 0;
        private String msg = "";
        private T data;
}
