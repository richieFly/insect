/*
 Navicat MySQL Data Transfer

 Source Server         : 182.61.134.185_百度8核16G10MB
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 182.61.134.185:3306
 Source Schema         : device

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 26/12/2020 10:49:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_camera
-- ----------------------------
DROP TABLE IF EXISTS `t_camera`;
CREATE TABLE `t_camera` (
                            `id` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '唯一ID',
                            `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
                            `app_key` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '应用key',
                            `app_secret` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '应用秘钥',
                            `device_serial` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '设备序列号',
                            `validate_code` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '设备验证码',
                            `access_token` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求令牌',
                            `expire_time` timestamp NULL DEFAULT NULL COMMENT 'token过期时间',
                            `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
                            `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='摄像头';

-- ----------------------------
-- Records of t_camera
-- ----------------------------
BEGIN;
INSERT INTO `t_camera` VALUES ('1', 'test', '61b0f556a7234c12ac968a72d1f68922', '3c4b26fc4dd7067b285ed696e49e7773', 'F17175722', 'JHUKYC', 'at.coznghwl6ph2mke894yqqaw763mgn89c-2o9zlx95v1-06exk4v-yvsuvjleo', '2020-12-30 23:20:19', '2020-12-23 13:33:42', '2020-12-23 13:33:49');
COMMIT;

-- ----------------------------
-- Table structure for t_device
-- ----------------------------
DROP TABLE IF EXISTS `t_device`;
CREATE TABLE `t_device` (
                            `device_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '唯一ID,设备ID',
                            `device_type` int(11) DEFAULT NULL COMMENT '设备类型编号',
                            `reboot` tinyint(1) DEFAULT NULL COMMENT '重启',
                            `signal_strength` double(20,0) DEFAULT NULL COMMENT '信号强度',
                            `battery_voltage` double(20,0) DEFAULT NULL COMMENT '电池电压-伏特',
                            `device_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '设备名称',
                            `solar_voltage` double(20,0) DEFAULT NULL COMMENT '太阳能电压-伏特',
                            `report_interval` int(11) DEFAULT NULL COMMENT '数据上传间隔',
                            `land_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '地块ID NULL',
                            `self_production_sm` tinyint(1) DEFAULT NULL COMMENT '产品序列码',
                            `axis_x` double(255,0) DEFAULT NULL COMMENT '经度',
                            `axis_y` double(255,0) DEFAULT NULL COMMENT '纬度',
                            `snap_interval` double(20,0) DEFAULT NULL COMMENT '数据采样结果',
                            `status` tinyint(1) DEFAULT NULL COMMENT '状态',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            PRIMARY KEY (`device_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_device
-- ----------------------------
BEGIN;
INSERT INTO `t_device` VALUES ('10012020000', 20, 0, 10, 9, 'test device', NULL, 3600, NULL, 1, NULL, NULL, NULL, 1, '2020-12-16 15:35:00', '2020-12-16 15:35:00');
COMMIT;

-- ----------------------------
-- Table structure for t_sensor
-- ----------------------------
DROP TABLE IF EXISTS `t_sensor`;
CREATE TABLE `t_sensor` (
                            `sensor_id` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '唯一标识',
                            `device_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '设备ID',
                            `alarm_type` tinyint(4) DEFAULT NULL COMMENT '报警类型',
                            `status` tinyint(1) DEFAULT NULL COMMENT '状态',
                            `sensor_type` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '传感器类型',
                            `name_en` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '传感器类型-英文名',
                            `name_cn` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '传感器类型-中文名',
                            `unit` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '传感器数据单位',
                            `data_value` double(255,0) DEFAULT NULL COMMENT '数据值',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`sensor_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_sensor
-- ----------------------------
BEGIN;
INSERT INTO `t_sensor` VALUES ('100120200001b0', '10012020000', 0, 1, 'B0', 'rain_day', '日降雨量', 'mm', 39, '2020-12-16 15:35:00', NULL);
INSERT INTO `t_sensor` VALUES ('100120200001b1', '10012020000', 0, 1, 'B1', 'air_water_1', '空气温度', '℃', 39, '2020-12-16 15:35:00', NULL);
INSERT INTO `t_sensor` VALUES ('100120200001b2', '10012020000', 0, 1, 'B2', 'air_water_2', '空气湿度', '%RH', 39, '2020-12-16 15:35:00', NULL);
INSERT INTO `t_sensor` VALUES ('100120200001b3', '10012020000', 0, 1, 'B3', 'der_temp', '露点温度', '℃', 39, '2020-12-16 15:35:00', NULL);
INSERT INTO `t_sensor` VALUES ('100120200001b4', '10012020000', 0, 1, 'B4', 'wind_direction', '风向', 'o', 39, '2020-12-16 15:35:00', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
