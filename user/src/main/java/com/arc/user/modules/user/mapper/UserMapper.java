package com.arc.user.modules.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.arc.user.modules.user.entity.UserEntity;

/**
 * @author axx
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
//
//	/**
//	 * 更新登录信息
//	 *
//	 * @param id
//	 *            用户id
//	 * @param loginIp
//	 *            登录的ip
//	 * @return 影响的条数
//	 */
//	Integer updateLoginInfo(@Param("id") String id, @Param("loginIp") String loginIp);
//
//	/**
//	 * 校验密码
//	 *
//	 * @param account
//	 *            帐号
//	 * @param password
//	 *            密文密码
//	 * @return 用户信息
//	 */
//	UserEntity checkAccountAndPwd(@Param("account") String account, @Param("password") String password);
//
//	/**
//	 * 修改密码
//	 *
//	 * @param param
//	 *            需要修改的用户信息，用户id/密码不允许为空
//	 * @return 影响的条数
//	 */
//	Integer updatePwd(UserEntity param);
//
//	/**
//	 * 根据用户id列表查询
//	 * @param ids 用户id列表
//	 * @return
//	 */
//	List<UserEntity> selectByIds(@Param("ids") String[] ids);
//
//	/**
//	 * 根据租户ID和账号
//	 * @param account 账号
//	 * @return
//	 */
//	List<UserEntity> selectByOrgIdAndAccount(@Param("account") String account);
}
