package com.arc.user.modules.user.service;

import com.arc.user.modules.user.entity.UserEntity;
import com.arc.user.modules.user.entity.UserWithTokenEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户token相关接口
 *
 * @author wjh
 *
 */
public interface ITokenService {

	/**
	 * 创建token：利用jwt生成token，并且存入redis设置过期时间； redis存储格式：user_token_用户id_用户token；
	 *
	 * @param account
	 *            帐号
	 * @param password
	 *            明文密码
	 * @param duration
	 *            过期时长，单位：秒
	 * @param clientIp
	 * 			  远程客户端ip
	 * @return 用户信息，包含token信息
	 */
	UserWithTokenEntity createToken(String account, String password, long duration, String clientIp);

	/**
	 * 校验token有效性，需要缓存处理
	 *
	 * @param request @
	 * @return 用户信息，包含权限信息
	 */
	UserEntity checkToken(HttpServletRequest request);

	/**
	 * 删除token信息
	 *
	 * @param request @
	 * @return 成功/失败
	 */
	boolean deleteToken(HttpServletRequest request);
}
