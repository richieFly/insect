package com.arc.user.modules.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * token信息
 * @author wjh
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class UserWithTokenEntity extends UserEntity{
	private static final long serialVersionUID = -7776998231354576058L;
	/**
	 * token信息
	 */
	private String token;
	
	/**
	 * 过期时间：单位秒
	 */
	private Long tokenExpireDuration;
	
}
