package com.arc.user.modules.user.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 用户实体
 * @author wjh
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("t_user")
public class UserEntity extends Model<UserEntity>{
	/**
	 * 主键
	 */
	@TableId(type = IdType.ASSIGN_UUID)
	private String id;

	/**
	 * 姓名
	 */
	private String name;

	/**
	 * 账号
	 */
	private String account;

	/**
	 * 旧密码
	 */
	@TableField(exist = false)
	private String oldPassword;

	/**
	 * 登录密码
	 */
	@JsonIgnore
	private String password;

	/**
	 * 确认密码
	 */
	@TableField(exist = false)
	private String checkPass;

	/**
	 * 头像地址
	 */
	private String headImg;

	/**
	 * 电话
	 */
	private String phone;

	/**
	 * 用户状态
	 */
	private Integer status;

	/**
	 * 最后登录ip
	 */
	private String lastLoginIp;

	/**
	 * 查询关键字
	 */
	@TableField(exist = false)
	private String keyWord;

	/**
	 * 最后登录时间
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	private Date lastLoginTime;

	/**
	 * 创建时间
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	/**
	 * 最后修改时间
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Shanghai")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

}






