package com.arc.user.modules.user.service.impl;

import com.arc.user.common.AccessParam;
import com.arc.user.common.ErrorCodeSuit.ErrorCode;
import com.arc.user.modules.user.entity.UserEntity;
import com.arc.user.modules.user.entity.UserStatus;
import com.arc.user.modules.user.entity.UserWithTokenEntity;
import com.arc.user.modules.user.service.ITokenService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dhcloud.avap.exception.ServiceException;
import com.dhcloud.avap.redis.RedisHelper;
import com.dhcloud.avap.web.util.UUIDUtil;
import com.google.common.base.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * token相关服务
 *
 * @author wjh
 *
 */
@Service
@Slf4j
public class TokenServiceImpl implements ITokenService {

	/**
	 * token删除缓存标识
	 */
	public static final String USER_DEL_TOKEN_FLAG = "user_del_token_";

	/**
	 * 默认时长
	 */
	@Value("${user.tokenDefaultExpire}")
	private int  tokenDefaultExpire;

	/**
	 * token 刷新时间
	 */
	@Value("${user.tokenRefreshTime}")
	private long  tokenRefreshTime;

	final RedisHelper redisHelper;
	@Autowired
	public TokenServiceImpl(RedisHelper redisHelper) {
		this.redisHelper = redisHelper;
	}

	@Override
	public UserWithTokenEntity createToken(String account, String password, long duration, String clientIp) {

		// 校验用户名和密码
		UserEntity curUserInfo = new UserEntity();
		curUserInfo = curUserInfo.selectOne(new QueryWrapper<>(new UserEntity().setPassword(password).setAccount(account)));

		if (curUserInfo == null) {
			throw new ServiceException(ErrorCode.ACCOUNT_OR_PWD_ERROR.getStatus());
		}

		/*
		 * 生成token，附带用户id/过期时间；使用用户密码作为密钥
		 */
		String token = signToken(curUserInfo, (int) duration);
		UserWithTokenEntity result = new UserWithTokenEntity(token,
				duration);
		try {
			BeanUtils.copyProperties(result, curUserInfo);
		} catch (Exception e) {
			log.error("复制属性异常", e);
		}
		/*
		 * 更新登录ip
		 */
		curUserInfo.setLastLoginIp(clientIp).updateById();

		return result;
	}

	private String signToken(UserEntity userInfo, int duration) {
		return JWT.create()
				.withAudience(userInfo.getId(), UUIDUtil.getUUID())
				.withClaim("account", userInfo.getAccount())
				.withExpiresAt(DateUtils.addSeconds(new Date(), duration))
				.sign(Algorithm.HMAC256(userInfo.getPassword()));
	}

	@Override
	public UserEntity checkToken(HttpServletRequest request) {
		String token = getTokenFromRequest(request);
		try {
			/*
			 * 从redis判断是否已被删除q
			 */
			if (redisHelper.exists(USER_DEL_TOKEN_FLAG + token)) {
				throw new ServiceException("token:" + token + ",expire");
			}

			/*
			 * 校验签名密钥：用户密码
			 */
			UserEntity userInfo = this.checkUserById(JWT.decode(token).getAudience().get(0));
			DecodedJWT jwt = JWT.require(Algorithm.HMAC256(userInfo.getPassword())).build().verify(token);

			// token过期时间小于${tokenRefreshTime}时, 将刷新token, 经response消息头返回
			Date expireTime = jwt.getExpiresAt();
			Date nowTime = new Date();
			if ((expireTime.getTime() - nowTime.getTime()) <= tokenRefreshTime * 1000) {
				String refreshToken = signToken(userInfo, tokenDefaultExpire);
				log.info("刷新token: {}", refreshToken);
				ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
				assert java.util.Objects.requireNonNull(servletRequestAttributes).getResponse() != null;
				assert servletRequestAttributes.getResponse() != null;
				servletRequestAttributes.getResponse().setHeader(HttpHeaders.AUTHORIZATION, refreshToken);
			}
			return userInfo;
		} catch (Exception e) {
			log.error("校验token:" + token + "异常", e);
			log.info("{},{}",ErrorCode.TOKEN_ERROR,ErrorCode.TOKEN_ERROR.getStatus());
			throw new ServiceException(ErrorCode.TOKEN_ERROR.getStatus());
		}

	}

	/**
	 * 获取有效用户
	 *
	 * @param userId @
	 * @return @
	 */
	private UserEntity checkUserById(String userId) {
		// 获取用户信息
		UserEntity curUserInfo = new UserEntity().selectById(userId);
		if (curUserInfo == null) {
			throw new ServiceException("user id not exist:" + userId);
		} else if (Objects.equal(curUserInfo.getStatus(), UserStatus.FORBIDDEN.getVal())) {
			throw new ServiceException("user id forbidden:" + userId);
		}
		return curUserInfo;
	}

	@Override
	public boolean deleteToken(HttpServletRequest request) {
		String token = getTokenFromRequest(request);
		try {
			//校验附带的过期时间
			Date expiresDate = JWT.decode(token).getExpiresAt();
			//记录至redis该token，用户校验时使用
			long expiresForRedis = (expiresDate.getTime()- System.currentTimeMillis())/1000L;
			if(expiresForRedis>0){
				redisHelper.set(USER_DEL_TOKEN_FLAG + token, expiresForRedis,expiresForRedis);
			}
			return true;
		} catch (Exception e) {
			log.error("删除token:" + token + "异常", e);
			return false;
		}
	}


	/**
	 * 从请求中获取token参数
	 *
	 * @param request @
	 * @return @
	 */
	private String getTokenFromRequest(HttpServletRequest request) {
		/*
		 * 先从头部获取token相关信息，如果没有再从参数中获取
		 */
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (StringUtils.isBlank(token)) {
			token = request.getParameter(AccessParam.ACCESS_TOKEN.toString());
		}
		/*
		 * 校验参数
		 */
		if (StringUtils.isBlank(token)) {
			log.error("token参数为空");
			throw new ServiceException(ErrorCode.TOKEN_ERROR.getStatus());
		}
		return token;
	}

}
