package com.arc.user.modules.user.rest;

import cn.hutool.crypto.SecureUtil;
import com.arc.user.common.ErrorCodeSuit;
import com.arc.user.common.IpUtils;
import com.arc.user.modules.user.entity.UserEntity;
import com.arc.user.modules.user.service.ITokenService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dhcloud.avap.exception.ServiceException;
import com.dhcloud.avap.operation.annotation.Operation;
import com.dhcloud.avap.status.BaseErrorCodeSuit;
import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 用户对外接口
 *
 * @author wjh
 *
 */
@RestController
@RequestMapping
public class UserRest extends RestControllerSupport {

	@Value("${user.defaultPassword:123456}")
	private String defaultPassword;
	/**
	 * 默认时长
	 */
	@Value("${user.tokenDefaultExpire}")
	private long tokenDefaultExpire;
	final ITokenService tokenService;
	@Autowired
	public UserRest(ITokenService tokenService) {
		this.tokenService = tokenService;
	}

	/**
	 * 创建token，对应登录
	 *
	 * @param account  帐号
	 * @param password 明文密码
	 * @param duration 有效期时长，单位：秒，最大不允许超过7天，默认为8个小时
	 * @return @
	 */
	@PostMapping("login")
	public RestResponse login(@RequestParam String account,
							  @RequestParam String password,
							  @RequestParam(required = false) Long duration,
							  HttpServletRequest request) {
		// 客户端传递的时长
		long durationNew = ((duration == null || duration < 1L) ? tokenDefaultExpire : duration);
		// 客户端ip
		String remoteClientIp = IpUtils.getIpAddress(request);
		return restResponses.ok(tokenService.createToken(account, password, durationNew, remoteClientIp));
	}

	/**
	 * 删除token，对应登出，头部或参数中获取token
	 *
	 * @return @
	 */
	@DeleteMapping("logout")
	public RestResponse delete(HttpServletRequest request) {
		if (tokenService.deleteToken(request)) {
			return restResponses.ok();
		}
		return restResponses.error();
	}

	/**
	 * 校验token，头部或参数中获取
	 *
	 * @return @
	 */
	@GetMapping("check")
	public RestResponse check(HttpServletRequest request) {
		return restResponses.ok(tokenService.checkToken(request));
	}

	/**
	 * 获取用户信息
	 *
	 * @return @
	 */
	@GetMapping("info")
	@Operation(value = "获取用户信息")
	public RestResponse info(HttpServletRequest request) {
		return restResponses.ok(tokenService.checkToken(request));
	}

	/**
	 * 分页获取当前配置的用户信息
	 *
	 * @param param @
	 * @param request @
	 * @return @
	 */
	@GetMapping("list")
	public RestResponse listByPage(UserEntity param, HttpServletRequest request) {
		int current = StringUtils.isNotBlank(request.getParameter("current")) ? Integer.parseInt(request.getParameter("current")) : 1 ;
		int size = StringUtils.isNotBlank(request.getParameter("size")) ? Integer.parseInt(request.getParameter("size")) : 10 ;
		QueryWrapper<UserEntity> query = new QueryWrapper<>();
		query.eq(Objects.nonNull(param.getStatus()), "status", param.getStatus());
		query.apply(StringUtils.isNotBlank(param.getKeyWord()),
				" ( name like '%"+param.getKeyWord()+"%' " +
						"or phone like '%"+param.getKeyWord()+"%'" +
						"or account like '%"+param.getKeyWord()+"%' " +
						"or last_login_ip like '%"+param.getKeyWord()+"%' )");

		return restResponses.ok(param.selectPage(new  Page<>( current,  size),query));
	}

	/**
	 * 用户信息新增
	 *
	 * @param param @
	 * @return @
	 */
	@PostMapping
	@Operation(value = " 用户信息新增/修改", paramNames = "param")
	public RestResponse add(@RequestBody UserEntity param) {
		return param.insert() ? restResponses.ok(param) : restResponses.error();
	}

	/**
	 * 用户信息新增/修改
	 *
	 * @param param @
	 * @return @
	 */
	@PutMapping
	@Operation(value = " 用户信息修改", paramNames = "param")
	public RestResponse update(@RequestBody UserEntity param) {
		if(StringUtils.isBlank(param.getId())){
			throw new ServiceException(BaseErrorCodeSuit.DefaultError.ILLEGAL_ARGUMENT.getStatus());
		}
		return param.updateById() ? restResponses.ok(param) : restResponses.error();
	}

	/**
	 * 删除用户信息，支持多个
	 *
	 * @param ids 用户ids
	 * @return @
	 */
	@DeleteMapping(value = "/{ids}")
	@Operation(value = "删除用户", paramNames = "ids")
	public RestResponse delete(@PathVariable String[] ids) {
		if (ArrayUtils.isEmpty(ids)) { return restResponses.error(BaseErrorCodeSuit.DefaultError.ILLEGAL_ARGUMENT.getStatus()); }
		for ( String id : ids) {
			new UserEntity().deleteById(id);
		}
		return restResponses.ok();
	}

	@PostMapping(value = "/updatePwd")
	@Operation(value = "更改密码", paramNames = "param")
	public RestResponse updatePwd(UserEntity param,HttpServletRequest request) {
	    UserEntity user = tokenService.checkToken(request);
        if (!param.getOldPassword().trim().equalsIgnoreCase(user.getPassword().trim())) {
			return restResponses.error(ErrorCodeSuit.ErrorCode.PASSWORD_OLD_ERROR.getStatus());
		}
		return param.updateById() ? restResponses.ok() :restResponses.error();
	}

	@PostMapping(value = "/password/reset")
	@Operation(value = "重置密码", paramNames = "param")
	public RestResponse passwordRest (UserEntity param,HttpServletRequest request) {
		UserEntity user = tokenService.checkToken(request);
		boolean res = false;
		if(Objects.nonNull(user)){
			res = param.setPassword(SecureUtil.md5(defaultPassword)).setId(user.getId()).updateById();
		}
		return res ? restResponses.ok(defaultPassword) :restResponses.error();
	}
}
