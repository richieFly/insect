package com.arc.user.modules.user.entity;

/**
 * 用户状态
 * @author wjh
 *
 */
public enum UserStatus {
	
	/**
	 * 正常
	 */
	NORMAL(1),
	
	/**
	 * 禁用
	 */
	FORBIDDEN(0);
	
	int val;
	
	UserStatus(int val){
		this.val = val;
	}
	
	public int getVal(){
		return val;
	}
}
