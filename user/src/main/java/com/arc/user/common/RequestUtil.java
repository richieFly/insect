package com.arc.user.common;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Convenience class for setting and retrieving cookies.
 * @author axx
 */
@Slf4j
public final class RequestUtil {
    public static final Integer SESSIONID_30D_AGE = 3600 * 24 * 30;

    public static final String IP_LOCAL="127.0.0.1";

    /**
     * Checkstyle rule: utility classes should not have public constructor
     */
    private RequestUtil() {
    }

    /**
     * get  HttpServletRequest....
     * @return
     */
    public static HttpServletRequest getRequest() {

        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

    }

    /**
     * Convenience method to set a cookie
     *
     * @param response the current response
     * @param name     the name of the cookie
     * @param value    the value of the cookie
     * @param maxAge   the max age of the cookie	default:30 days
     * @param path     the path to set it on
     */
    @SuppressWarnings({
        "squid:S2092"
    })
    public static void setCookie(HttpServletResponse response, String name,
                                 String value, Integer maxAge, String path) {
        if (log.isDebugEnabled()) {
            log.debug("Setting cookie" + name + "on path" + path + "'");
        }

        Cookie cookie = new Cookie(name, value);
        cookie.setSecure(false);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge); // default:30 days

        response.addCookie(cookie);
    }

    public static void setCookie(HttpServletResponse response, String name, String value, Integer maxAge) {
        setCookie(response, name, value, maxAge, "/");
    }

    public static void setCookie(HttpServletResponse response, String name, String value) {
        setCookie(response, name, value, SESSIONID_30D_AGE, "/");
    }

    @SuppressWarnings({
      "squid:S2092"
    })
    public static void setHttpOnlyCookie(HttpServletResponse response, String name,
                                         String value, Integer maxAge, String path) {
        if (log.isDebugEnabled()) {
            log.debug("Setting setHttpOnlyCookie" + name + "on path1" + path + "'");
        }

        Cookie cookie = new Cookie(name, value);
        cookie.setSecure(false);
        cookie.setPath(path);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    @SuppressWarnings({
      "squid:S2092"
    })
    public static void setHttpOnlyCookie(HttpServletResponse response, String name,
                                         String value, String path) {
        if (log.isDebugEnabled()) {
            log.debug("Setting setHttpOnlyCookie2 '" + name + "on path2" + path + "'");
        }

        Cookie cookie = new Cookie(name, value);
        cookie.setSecure(false);
        cookie.setPath(path);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(3600 * 24 * 30); // default:30 days
        response.addCookie(cookie);
    }

    /**
     * Convenience method to get a cookie by name
     *
     * @param request the current request
     * @param name    the name of the cookie to find
     * @return the cookie (if found), null if not found
     */
    public static Cookie getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        Cookie returnCookie = null;

        if (cookies == null) {
            return returnCookie;
        }

        for (Cookie thisCookie : cookies) {
            if ((name).equals(thisCookie.getName())&&!("").equals(thisCookie.getValue())) {
                    returnCookie = thisCookie;
                    break;
            }
        }

        return returnCookie;
    }

    /**
     * Convenience method for deleting a cookie by name
     *
     * @param response the current web response
     * @param cookie   the cookie to delete
     * @param path     the path on which the cookie was set (i.e. /appfuse)
     */
    public static void deleteCookie(HttpServletResponse response,
                                    Cookie cookie, String path) {
        if (cookie != null) {
            // Delete the cookie by setting its maximum age to zero
            cookie.setMaxAge(0);
            cookie.setPath(path);
            response.addCookie(cookie);
        }
    }



    /**
     * 获取客户端IP地址，此方法用在proxy环境中
     *
     * @param req
     * @return
     */
    public static String getRemoteAddr(HttpServletRequest req) {
        String ip = req.getHeader("X-Forwarded-For");
        if (StringUtils.isNotBlank(ip)) {
            String[] ips = StringUtils.split(ip, ',');
                for (String tmpip : ips) {
                    if (StringUtils.isBlank(tmpip)) {
                        continue;
                    }
                    tmpip = tmpip.trim();
                    if (isIpAddr(tmpip) &&
                            !tmpip.startsWith("10.") &&
                            !tmpip.startsWith("192.168.") && !IP_LOCAL.equals(tmpip)) {
                        return tmpip.trim();
                    }
                }
        }
        ip = req.getHeader("x-real-ip");
        if (isIpAddr(ip)){
            return ip;
        }
        ip = req.getRemoteAddr();
        if (isIpAddr(ip)) {
            return ip;
        }
        return IP_LOCAL;
    }

    /**
     * 判断字符串是否是一个IP地址
     *
     * @param addr
     * @return
     */
    public static boolean isIpAddr(String addr) {
        if (StringUtils.isEmpty(addr)) {
            return false;
        }
        String[] ips = StringUtils.split(addr, '.');
        if (ips.length != 4) {
            return false;
        }
        try {
            int ipa = Integer.parseInt(ips[0]);
            int ipb = Integer.parseInt(ips[1]);
            int ipc = Integer.parseInt(ips[2]);
            int ipd = Integer.parseInt(ips[3]);
            return ipa >= 0 && ipa <= 255 && ipb >= 0 && ipb <= 255 && ipc >= 0
                    && ipc <= 255 && ipd >= 0 && ipd <= 255;
        } catch (Exception e) {
            return false;
        }
    }

}
