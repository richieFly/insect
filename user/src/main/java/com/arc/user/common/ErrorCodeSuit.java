package com.arc.user.common;

import com.dhcloud.avap.status.BaseErrorCodeSuit;
import com.dhcloud.avap.status.DefaultStatus;
import com.dhcloud.avap.status.Status;
import org.springframework.stereotype.Component;

/**
 * 用户模块错误码，12开头，后跟三位错误码
 *
 * @author wjh
 */
@Component
public class ErrorCodeSuit extends BaseErrorCodeSuit {
    public enum ErrorCode {

        /**
         * 帐号或者密码错误
         */
        ACCOUNT_OR_PWD_ERROR(52917001),

        /**
         * token无效
         */
        TOKEN_ERROR(52917002),
        /**
         * 两次密码不一致
         */
        PASSWORD_INCONSISTENT(52917003),

        /**
         * 不能删除自己的账号
         */
        USER_NOT_DEL_SELF(52917006),

        /**
         * id列表不能为空
         */
        IDS_NOT_EMPTY(52917007),

        /**
         * 账号已存在
         */
        USER_ACCOUNT_EXISTED(52917014),

        /**
         * 旧密码错误
         */
        PASSWORD_OLD_ERROR(52917015),

        ;

        /**
         * 组装错误码
         *
         * @return @
         */
        public Status getStatus() {
            return DefaultStatus.toStatus(this.code, message(this.name()));
        }

        /**
         * 错误码
         */
        private final Integer code;

        public Integer getCode() {
            return code;
        }

        /**
         * 构造方法
         *
         * @param code @
         */
        ErrorCode(Integer code) {
            this.code = code;
        }

    }
}