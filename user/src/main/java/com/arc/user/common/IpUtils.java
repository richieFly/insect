package com.arc.user.common;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**
 * 网络相关工具类
 * @author wjh
 *
 */
public class IpUtils {
	private IpUtils(){}
	/**
	 * 获取客户端ip，兼容部分http头协议
	 *
	 * @param request @
	 * @return @
	 */
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		String unknown = "unknown";
		if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase(unknown, ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase(unknown, ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase(unknown, ip)) {
			ip = request.getRemoteAddr();
		}
		if (StringUtils.isBlank(ip) || StringUtils.equalsIgnoreCase(unknown, ip)) {
			return null;
		}
		if (ip.contains(",")) {
			return ip.split(",")[0];
		} else {
			return ip;
		}
	}
}
