package com.arc.user.common;

/**
 * 网关固定请求&传递参数
 * 
 * @author wjh
 *
 */
public enum AccessParam {
	/**
	 * 当前用户的token信息
	 */
	ACCESS_TOKEN("accessToken"), 
	
	/**
	 * 内部传递的用户id
	 */
	ACCESS_USER_ID("accessUserId")

	;

	private String paramName;

	AccessParam(String paramName) {
		this.paramName = paramName;
	}

	@Override
	public String toString() {
		return this.paramName;
	}

}
