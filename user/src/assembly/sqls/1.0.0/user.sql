/*
 Navicat MySQL Data Transfer

 Source Server         : 182.61.134.185_百度8核16G10MB
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 182.61.134.185:3306
 Source Schema         : user

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 26/12/2020 10:50:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '唯一ID',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
  `account` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '登录账号',
  `password` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '登录密码（MD5加密后）',
  `head_img` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '头像URL',
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) DEFAULT NULL COMMENT '用户状态',
  `last_login_ip` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '登录IP',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

SET FOREIGN_KEY_CHECKS = 1;
