//package com.arc.zuul.inserservice;
//
//import feign.Response;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//
///**
// * 用户相关
// * @author axx
// */
//@FeignClient(name = "user")
//public interface UserClient {
//    /**
//     * 调用hls停止录制任务
//     *
//     * @return @
//     */
//    @GetMapping(path = "check")
//    Response check();
//}
