package com.arc.zuul.inserservice.entity;

import lombok.Data;

/**
 * 用户实体
 * @author wjh
 *
 */
@Data
public class UserEntity{
	/**
	 * 主键
	 */
	private String id;

	/**
	 * 账号
	 */
	private String account;
}






