package com.arc.zuul.interceptor;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author axx
 */
@Component
public class AuthorityInterceptor extends ZuulFilter {

//    final UserClient userClient;
//    @Autowired
//    public AuthorityInterceptor(UserClient userClient) {
//        this.userClient = userClient;
//    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        //得到request上下文
        RequestContext currentContext = RequestContext.getCurrentContext();
        //得到request域
        HttpServletRequest request = currentContext.getRequest();
        //得到头信息
        String token = request.getHeader("Authorization");
        //判断是否有头信息
        if(token!=null && !"".equals(token)){
            //把头信息继续向下传
            currentContext.addZuulRequestHeader("Authorization", token);
        }
        currentContext.setRequest(request);
        // 放行zuul的第一次请求 todo 我并没有触发这个方法的执行
        String options = "OPTIONS";
        if (options.equals(request.getMethod())){
            System.err.println(options);
            return null;
        }

        // 放行登录请求
        String login = "login";
        if (request.getRequestURL().indexOf(login)>0){
            return null;
        }

//        if (StringUtils.isNotBlank(token)){
//                if (StringUtils.isNotBlank(token)){
//                    System.out.println("token==="+token);
//                    try{
//                        Response response = userClient.check();
//                        if(response.status() == 0 && Objects.nonNull(response.body())){
//                            // 参数中塞入userId
//                            Map<String, List<String>> requestQueryParams = currentContext.getRequestQueryParams();
//                            if (requestQueryParams==null) {
//                                requestQueryParams=new HashMap<>(1);
//                            }
//                            ArrayList<String> arrayList = new ArrayList<>();
//                            arrayList.add(((UserEntity)response.body()).getId());
//                            requestQueryParams.put("userId", arrayList);
//
//                            currentContext.setRequestQueryParams(requestQueryParams);
//                            return null;
//                        }else{
//                            // 其他情况, 终止访问
//                            currentContext.setSendZuulResponse(false);
//                        }
//                        currentContext.setSendZuulResponse(false);
//                    }catch (Exception e){
//                        // 解析token出现的异常,说明token有问题, 终止本次请求
//                        System.out.println("token出错了,终止本次访问: "+e);
//                        currentContext.setSendZuulResponse(false);
//                    }
//                }
//        }
//        currentContext.setSendZuulResponse(false);
        currentContext.getResponse().setContentType("text/html;chatset=utf-8");
//        try {
//            currentContext.getResponse().getWriter().write("权限不足");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return currentContext;
    }
}
