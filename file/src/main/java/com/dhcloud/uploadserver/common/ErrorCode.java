package com.dhcloud.uploadserver.common;


import com.dhcloud.avap.status.DefaultStatus;
import com.dhcloud.avap.status.Status;

/**
 * 上传模块错误码，1035开头
 * @author dcz
 *
 */
public enum ErrorCode {
	
	
	/*
	 * ##################################通用错误begin##################################
	 */
	/**
	 * 操作失败
	 */
	ERROR(10351111),
	/**
	 * 对象不存在
	 */
	OBJECT_NOT_EXIST(10350001),
	/**
	 * 对象已存在
	 */
	OBJECT_ALREADY_EXIST(10350002),
	/**
	 * 检测到非法参数，请确认参数传递是否正确
	 */
	ILLEGAL_ARGUMENT(10350003),
	/**
	 * 操作数据库异常
	 */
	DB_ERROR(10350004),
	/*
	 * ##################################通用错误end##################################
	 */
	
	/*
	 * ##################################下载错误begin##################################
	 */

	;
	/*
	 * ##################################上传错误end##################################
	 */
	
	
	/**
	 * 组装错误码
	 * @return
	 */
	public Status getStatus(){
    	return DefaultStatus.toStatus(this.code,this.name());
    }
	
	/**
	 * 错误码
	 */
	private Integer code; 
	
	
	
	 public Integer getCode() {
		return code;
	}


	/**
	  * 构造方法  
	  * @param code
	  */
    ErrorCode(Integer code) {
        this.code = code;  
    }
    
}
