//package com.dhcloud.uploadserver.interceptor;
//
//import java.util.Optional;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.lang.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import com.dhcloud.avap.web.rest.RestResultResponse;
//import com.dhcloud.avap.web.util.ThreadLocalManager;
//import com.dhcloud.uploadserver.common.Constants;
//import com.dhcloud.uploadserver.innerservice.UserFeign;
//import com.dhcloud.uploadserver.innerservice.entity.UserEntity;
//
///**
// * 校验token
// *
// * @author dcz
// */
//public class TokenInterceptor implements HandlerInterceptor {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(TokenInterceptor.class);
//
//	@Autowired
//	private UserFeign userService;
//
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
//		StringBuffer url = request.getRequestURL();
//		request.setCharacterEncoding(Constants.DEFAULT_ENCODING_UTF8);
//
//		String token = Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
//				.orElseGet(() -> Optional.ofNullable(request.getParameter(Constants.PARAM_NAME_TOKEN)).orElse(request.getHeader(Constants.PARAM_NAME_TOKEN)));
//
//		if (StringUtils.isBlank(token)) {
//			response.setStatus(HttpStatus.UNAUTHORIZED.value());
//			LOGGER.error("UNAUTHORIZED:token  {} when accessing:{}.", token, url);
//			return false;
//		}
//		LOGGER.info("SignatureInterceptor:token {} when accessing:{}.", token, url);
//
//		try {
//			RestResultResponse<UserEntity> res = userService.checkToken(token);
//			if (res.isSuccess()) {
//				ThreadLocalManager.setValue(Constants.PARAM_NAME_USERID, res.getResult().getId());
//				ThreadLocalManager.setValue(Constants.PARAM_NAME_DOMAIN_PREFIX, res.getResult().getDomainPrefix());
//				ThreadLocalManager.setValue(Constants.PARAM_NAME_TOKEN, token);
//				return true;
//			} else {
//				LOGGER.error("check user token:{}, error,{},when accessing:{}", token, res, url);
//			}
//		} catch (Exception e) {
//			LOGGER.error("get user info by token:" + token + " error,when accessing:" + url, e);
//		}
//
//		response.setStatus(HttpStatus.UNAUTHORIZED.value());
//		LOGGER.error("UNAUTHORIZED:token {} when accessing:{}, error.", token, url);
//
//		return false;
//	}
//
//	@Override
//	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
//		ThreadLocalManager.removeValue(Constants.PARAM_NAME_USERID);
//		ThreadLocalManager.removeValue(Constants.PARAM_NAME_DOMAIN_PREFIX);
//		ThreadLocalManager.removeValue(Constants.PARAM_NAME_TOKEN);
//	}
//
//	@Override
//	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
//		// do nothing
//	}
//
//}
