package com.dhcloud.uploadserver.modules.download.rest;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dhcloud.avap.web.RestControllerSupport;
import com.dhcloud.avap.web.rest.RestResponse;
import com.dhcloud.uploadserver.modules.download.service.IDownloadFileService;

/**
 * 下载服务对外接口
 *
 * @author dcz
 */
@RestController
@RequestMapping(value = "/download")
public class DownloadRest extends RestControllerSupport {


    final IDownloadFileService downloadService;
    @Autowired
    public DownloadRest(IDownloadFileService downloadService) {
        this.downloadService = downloadService;
    }

    /**
     * 下载接口
     * <p>
     * 请求参数
     *
     * @return
     */
    @PostMapping
    public RestResponse download(String[] urls) {
         downloadService.downloadFiles(urls);
        /*
         * 调用下载服务
         */
        return restResponses.ok();
    }

}
