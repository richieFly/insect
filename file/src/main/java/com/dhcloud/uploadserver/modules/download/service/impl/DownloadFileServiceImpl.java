package com.dhcloud.uploadserver.modules.download.service.impl;

import com.dhcloud.avap.exception.ServiceException;
import com.dhcloud.uploadserver.modules.download.service.IDownloadFileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

/**
 * @author axx
 */
@Service
public class DownloadFileServiceImpl implements IDownloadFileService {
    @Value("${file.images}")
    private String savePath;
    /**
     * 文件下载超时时间-毫秒
     */
    private static final int MAX_TIME_OUT = 5000;

    @Override
    public void downloadFiles(String[] fileUrls) {
        Arrays.stream(fileUrls).forEach(this::downLoadOneUrl);
    }

    private void downLoadOneUrl(String fileUrl){
        try {
            String fileName = fileUrl.substring(fileUrl.lastIndexOf('/'));
            File file = new File(savePath + fileName);
            file.createNewFile();
            URL url = new URL(fileUrl);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setConnectTimeout(MAX_TIME_OUT);
            urlCon.setReadTimeout(MAX_TIME_OUT);
            int code = urlCon.getResponseCode();
            if (code != HttpURLConnection.HTTP_OK) {
                throw new ServiceException("文件读取失败");
            }
            DataInputStream in = new DataInputStream(urlCon.getInputStream());
            DataOutputStream out = new DataOutputStream(new FileOutputStream(savePath + fileName));
            byte[] buffer = new byte[2048];
            int count;
            while ((count = in.read(buffer)) > 0) {
                out.write(buffer, 0, count);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            throw new ServiceException("文件下载失败", e);
        }
    }
}
