package com.dhcloud.uploadserver.modules.download.service;

/**
 * 下载服务接口
 *
 * @author dcz
 */
public interface IDownloadFileService {

    /**
     * 下载文件
     *
     * @param fileUrl @
     */
    void downloadFiles(String[] fileUrl);

}
