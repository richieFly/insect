package com.dhcloud.uploadserver.innerservice.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户实体
 * @author dcz
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity implements Serializable{

	private static final long serialVersionUID = -7676998231354576058L;

	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 父级id
	 */
	private String parentId;

	/**
	 * 姓名
	 */
	private String name;

	/**
	 * 登录帐号
	 */
	private String account;

	/**
	 * 用户域名前缀（创建时随机生成，后续不会改变）
	 */
	private String domainPrefix;

	/**
	 * 电话
	 */
	private String phone;

	/**
	 * 用户状态（0-禁用、1-正式、2-试用）
	 */
	private Integer status;
	
	/**
	 * 用户类型（默认0，其他待定）
	 */
	private Integer type;
	
	/**
	 * 用户来源（默认0，其他待定）
	 */
	private Integer source;
	
	/**
	 * 备注说明
	 */
	private String remarks;

}
