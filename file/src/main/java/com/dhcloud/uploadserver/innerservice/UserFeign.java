package com.dhcloud.uploadserver.innerservice;

import org.apache.http.HttpHeaders;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.dhcloud.avap.web.rest.RestResultResponse;
import com.dhcloud.uploadserver.innerservice.entity.UserEntity;

/**
 * @author axx
 */
@FeignClient(name = "sx-user")
public interface UserFeign {

	/**
	 * 校验token
	 *
	 * @param token
	 *            待验证的token信息
	 * @return token所属的用户信息
	 */
	@GetMapping(path = "/token/check")
	RestResultResponse<UserEntity> checkToken(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

	/**
	 * 根据用户id获取用户信息
	 *
	 * @param id
	 *            用户id
	 * @return 用户信息
	 */
	@GetMapping(path = "/user/info/{userId}")
	RestResultResponse<UserEntity> getUserById(@RequestParam("userId") String id);
	/**
	 * 获取用户属性配置
	 *
	 * @param userId
	 *            用户id
	 * @param name
	 *            属性名称
	 * @return 属性值
	 */
	@GetMapping(path = "/user/property/{userId}/{name}")
	RestResultResponse<String> getUserProperty(@PathVariable("userId") String userId, @PathVariable("name") String name);

}
