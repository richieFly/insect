//package com.dhcloud.uploadserver.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import com.dhcloud.uploadserver.interceptor.TokenInterceptor;
//
//@Configuration
//public class MyWebAppConfigurer implements WebMvcConfigurer{
//
//	@Override
//	public void addInterceptors(InterceptorRegistry registry) {
//		// 多个拦截器组成一个拦截器链
//		// addPathPatterns 用于添加拦截规则
//		// excludePathPatterns 用户排除拦截
//		registry.addInterceptor(tokenInterceptor()).addPathPatterns("/**").excludePathPatterns("/taskManage/**");
//	}
//
//	@Bean
//	public TokenInterceptor tokenInterceptor(){
//		return new TokenInterceptor();
//	}
//}
