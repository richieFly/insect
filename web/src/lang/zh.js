export default {
  route: {
    dashboard: '首页',

    userManage: '用户管理',
    userList: '用户列表',

    deviceManage: '设备管理管理',
    cameraManage: '摄像头管理',
    sensorManage: '传感器管理',

    errorPages: '错误页面',
    page401: '401',
    page404: '404'
  },
  common:{
    keyWord: '请输入关键字',
    id: '唯一标识',
    name: '名称',
    createTime: '创建时间',
    updateTime: '更新时间',
    search: '搜索',
    actions: '操作',
    add: '添加',
    edit: '编辑',
    delete: '删除',
    confirm: '确定',
    cancel: '取消',
  },
  navbar: {
    dashboard: '首页',
    logOut: '退出登录',
    size: '布局大小'
  },
  login: {
    title: '系统登录',
    logIn: '登录',
    username: '账号',
    password: '密码',
  },
  camera:{
    controller: '摄像头控制',
    appKey: '应用Key',
    appSecret: '访问秘钥',
    deviceSerial: '设备序列号',
    validateCode: '验证码',
  },
  sensor:{
    controller: '摄像头控制',
    appKey: '应用Key',
    appSecret: '访问秘钥',
    deviceSerial: '设备序列号',
    validateCode: '验证码',
  },
  user:{
    id: '序号',
    account: '账号',
    name: '姓名',
    phone: '手机',
    status: '状态',
    enable: '启动',
    disable: '禁用',
    lastLoginIp: '最后登录IP',
    lastLoginTime: '最后登录时间',
  },
  settings: {
    title: '系统布局配置',
    theme: '主题色',
    tagsView: '开启 Tags-View',
    fixedHeader: '固定 Header',
    sidebarLogo: '侧边栏 Logo'
  }
}
