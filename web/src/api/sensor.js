import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/device/sensor',
    method: 'get',
    params: query
  })
}


export function create(data) {
  return request({
    url: `/device/sensor`,
    data:data,
    method: 'POST'
  })
}

export function update(data) {
  return request({
    url: `/device/sensor`,
    data: data,
    method: 'PUT'
  })
}
export function del(ids) {
  return request({
    url: `/device/sensor/${ids}`,
    method: 'delete'
  })
}
