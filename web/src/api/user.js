import request from '@/utils/request'
import md5 from 'js-md5'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    params: {
      account: data.username,
      password: data.password
    }
  })
}
export function fetchList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}

export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: `/user`,
    data:data,
    method: 'POST'
  })
}

export function update(data) {
  console.log(data +"YYYYYYYYY")
  return request({
    url: `/user`,
    data: data,
    method: 'PUT'
  })
}



export function del(ids) {
  return request({
    url: `/user/${ids}`,
    method: 'delete'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'delete'
  })
}
