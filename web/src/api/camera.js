import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/device/camera',
    method: 'get',
    params: query
  })
}

export function create(data) {
  return request({
    url: `/device/camera`,
    data:data,
    method: 'POST'
  })
}

export function update(data) {
  return request({
    url: `/device/camera`,
    data: data,
    method: 'PUT'
  })
}
export function del(ids) {
  return request({
    url: `/device/camera/${ids}`,
    method: 'delete'
  })
}

export function getPlayUrl(data) {
  return request({
    url: '/device/camera/address',
    method: 'get',
    params: {
      id: data.id,
      protocol:1,
      expireTime:72000
    }
  })
}


export function mirror(id) {
  return request({
    url: `/device/camera/mirror?id=${id}`,
    method: 'post'
  })
}

export function move(data) {
  return request({
    url: '/device/camera/start',
    method: 'post',
    params: {
      id: data.id,
      channelNo: data.channelNo,
      direction: data.direction,
      speed: data.speed
    }
  })
}

export function stop(data) {
  return request({
    url: '/device/camera/stop',
    method: 'post',
    params: {
      id: data.id,
      channelNo: data.channelNo,
      direction: data.direction
    }
  })

}
