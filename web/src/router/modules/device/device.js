import Layout from '@/layout'

const deviceRouter = {
    path: '/device',
    component: Layout,
    redirect: '/device',
    name: 'device',
    meta: {
      title: 'deviceManage',
      icon: 'el-icon-coordinate'
    },
    alwaysShow:true,
    children: [
      {
        path: 'camera',
        component: () => import('@/views/device/camera'),
        name: 'cameraManage',
        meta: { title: 'cameraManage', icon: 'el-icon-video-camera' }
      },
      {
        path: 'sensor',
        component: () => import('@/views/device/sensor'),
        name: 'sensorManage',
        meta: { title: 'sensorManage', icon: 'el-icon-cpu' }
      }
    ]
  }

export default deviceRouter
