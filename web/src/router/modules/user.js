/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const userRouter = {
    path: '/user',
    component: Layout,
    redirect: '/user/list',
    name: 'userManage',
    meta: {
      title: 'userManage',
      icon: 'el-icon-user'
    },
    alwaysShow:true,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/list'),
        name: 'userList',
        meta: { title: 'userList', icon: 'list' }
      }
    ]
  }

export default userRouter
